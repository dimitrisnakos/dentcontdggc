#!/usr/bin/env python3

###############################################################################
#                           AdaptiveIF model BRIAN2                           #
#                               active dendrite                               #
#                                    via                                      #
#                                custom events                                #
#                                with SYNAPSES                                #
#                                 VALIDATION                                  #
#                     (somatic EPSP Amplitude and ratio)                      #
###############################################################################

from brian2 import *
import matplotlib.pyplot as plt
import numpy as np

# factor function used for normalization and min(=0), max(=1) during s_{AMPA,NMDA} graphs.
def factor(tau_rise,tau_decay):
    # Calculate the time @ peak
    tpeak = ((tau_decay*tau_rise) / (tau_decay-tau_rise)) * np.log(tau_decay/tau_rise)
    # Calculate the peak value - maximum value
    smax = ((tau_decay*tau_rise) / (tau_decay-tau_rise))*( np.exp(-(tpeak)/tau_decay) - np.exp(-(tpeak)/tau_rise) ) / ms
    
    return (1/smax)


###############################################################################
#                 N e u r o n             p a r a m e t e r s                 #
###############################################################################

start_scope()

## population
N_GC = 1 # GC neurons of DG

## soma
Elsoma  = -80 * mV
Csoma 	= 70.* pfarad
glsoma 	= (Csoma / (26.9 * ms)) / 2.3 
Vreset  = -74. * mV
Vthres  = 56.432 * mV 
print ('Soma Membrane time constant (Cm/gl) is : ', Csoma/glsoma)

## dendrite
Eldend  = (Elsoma + 5 * mV) 
gldend  = 2. * glsoma
Cdend   = 2. * Csoma
print ('Dendrite membrane time (Cm/gl) constant is : ', Cdend/gldend)

## Global
gc      = 6.8 * nS 

## AdIF
alpha   = 1.35 * nS
tauw    = 45. * ms  
beta    = .045 * nA 

## Na & K channels
tauNa   = 3. * ms
tauK = 6. * ms

### AMPA (excitatory)
E_AMPA      = 0. * mV
g_AMPA_ext  = 2.1066 * nS
tau_AMPA_rise     = .073 * ms
tau_AMPA_decay    = 2. * ms
alpha_AMPA        = .68 / ms

### NMDA (excitatory)
E_NMDA            = 0. * mV
g_NMDA_ext        = .99 * g_AMPA_ext 
tau_NMDA_rise     = .33 * ms
tau_NMDA_decay    = 50. * ms
alpha_NMDA        = .22 / ms
Mg2               = 2.
heta              = .2
gamma             = .04

## Neuron irrelevant
dt = defaultclock.dt
fampa = factor(tau_AMPA_rise,tau_AMPA_decay)
fnmda = factor(tau_NMDA_rise,tau_NMDA_decay)

###############################################################################
#                                M o d e l                                    #
###############################################################################

eqs_gc = '''
    dVsoma/dt = (-glsoma * (Vsoma - Elsoma) - gc * (Vsoma - Vdend) - w + Iextsoma ) / Csoma : volt (unless refractory)    
    dw/dt = (alpha * (Vsoma -Elsoma) -w ) / tauw : amp
    
    dVdend/dt = (-gldend * (Vdend - Eldend) -gc * (Vdend - Vsoma) + INa + IK + Iextdend - Isyn) / Cdend :volt
    dINa/dt = -INa / tauNa : amp
    dIK/dt = -IK / tauK : amp
    
    Isyn = I_AMPA_ext + I_NMDA_ext : amp
    
    I_AMPA_ext = g_AMPA_ext * (Vdend - E_AMPA) * s_AMPA_ext_tot : amp
    s_AMPA_ext_tot : 1
    
    I_NMDA_ext = g_NMDA_ext * (Vdend - E_NMDA) / (1 + Mg2 / heta * exp(gamma * Vdend / mV)) * s_NMDA_ext_tot : amp
    s_NMDA_ext_tot : 1
    
    Iextsoma : amp
    Iextdend : amp
    allow_dspike : boolean
    '''

granule = NeuronGroup(1, model = eqs_gc, 
                     threshold = 'Vsoma > Vthres',
                     reset = 'Vsoma = Vreset; w += beta',
                     refractory= 2. * ms,
                     events = {
                     'dspike' : 'Vdend > -55 * mV and allow_dspike',
                     'drop_dspike' : 'Vdend > -37 * mV and not allow_dspike',
                     'force_true_dspike' : 'IK > -.009 * pA and not allow_dspike'
                     },
                     method = 'rk4'
                     )

# custom events conditions
granule.run_on_event('dspike', 'INa = 1000 * pA; allow_dspike = False')
granule.run_on_event('drop_dspike', 'IK = - 1600 * pA')
granule.run_on_event('force_true_dspike', 'allow_dspike = True')

###############################################################################
#                             S Y N A P S E S                                 #
###############################################################################

# For validation only one spike needs to be used.

indices = array([0])
times = array([400]) * ms
spike_inp = SpikeGeneratorGroup(1, indices, times)

synGlut = Synapses(spike_inp, granule,
                   model = """
                   s_AMPA_ext_tot_post = s_AMPA_ext : 1 (summed)
                   ds_AMPA_ext / dt = - s_AMPA_ext / tau_AMPA_decay + alpha_AMPA * x_AMPA * (1 - s_AMPA_ext) : 1 (clock-driven)
                   dx_AMPA / dt = - x_AMPA / tau_AMPA_rise : 1 (clock-driven)
                   
                   s_NMDA_ext_tot_post = s_NMDA_ext : 1 (summed)
                   ds_NMDA_ext / dt = - s_NMDA_ext / tau_NMDA_decay + alpha_NMDA * x_NMDA * (1 - s_NMDA_ext) : 1 (clock-driven)
                   dx_NMDA / dt = - x_NMDA / tau_NMDA_rise : 1 (clock-driven)
                   """,
                   on_pre = 'x_AMPA += 1; x_NMDA += 1',
                   method = 'rk4'
                   )

synGlut.connect()

# initializing all values.
granule.Vsoma = Elsoma
granule.Vdend = Eldend
granule.allow_dspike = True

###############################################################################
#                             M O N I T O R I N G                             #
###############################################################################

spk = SpikeMonitor(granule)

mon = StateMonitor(granule, ('Vsoma', 'Vdend', 'I_AMPA_ext', 'I_NMDA_ext'), record=True)
run(300 *ms)

granule.Iextsoma = 0. * nA
granule.Iextdend = 0. * nA
run(50 * ms)

granule.Iextsoma = 0. * nA
granule.Iextdend = 0. * nA
run(50 * ms)

granule.Iextsoma = 0. * nA
granule.Iextdend = 0. * nA
run(200 * ms)

###############################################################################
#                        D a t a     G a t h e r i n g                        #
###############################################################################

EPSP_amp = max(mon[0].Vsoma[2500:]) - min(mon[0].Vsoma[2500:])
print ("AMPA and NMDA provokes a", EPSP_amp, "somatic EPSP.")
print ("ratio of I_NMDA/I_AMPA is: ", max(abs(mon[0].I_NMDA_ext)) / max(abs(mon[0].I_AMPA_ext)))

## In case I want to save these at an external .txt file I uncomment the following:
# np.savetxt('synvalidationNMDA_dend.txt', mon[0].Vdend, header='data to use for doing syn validation (dendrite)')
# np.savetxt('synvalidationNMDA_soma.txt', mon[0].Vsoma, header='data to use for doing syn validation (soma)')

###############################################################################
#                              P l o t t i n g                                #
###############################################################################
      
### Soma and dendrite plot
plt.figure()
plt.plot()
plt.plot(mon.t[3000:] / ms, mon[0].Vsoma[3000:] / mV, label='soma', c='black')
plt.text(460, -77.86, '0.63 mV', fontsize = 19)
plt.vlines(451, -77.3445, -77.96925, colors='c',linestyles='dashed', label='amplitude')
plt.xlabel('Time (ms)', fontsize = 12)
plt.ylabel('EPSP (mV)', fontsize = 12)
plt.title('Somatic EPSP')
plt.legend(frameon = False, loc = 4)


# plt.figure() #### <---- Debugging
# plt.plot()
# # plt.subplot(212)
# # plt.tight_layout()
# plt.plot(mon.t / ms, mon[0].I_AMPA_ext[:] / pA, label='AMPA', c='b')
# plt.plot(mon.t / ms, mon[0].I_NMDA_ext[:] / pA, label='NMDA', c='g')
# plt.xlabel('Time (ms)', fontsize = 12)
# plt.ylabel('EPSC (pA)', fontsize = 12)
# # plt.ylim([-25, 0])
# plt.legend()


# plt.figure() #### <---- Debugging
# plt.plot()
# # plt.subplot(211)
# # plt.tight_layout()
# plt.plot(mon.t / ms, mon[0].s_AMPA_ext[:] * fampa, label='AMPA', c='blue')
# plt.plot(mon.t / ms, mon[0].s_NMDA_ext[:] * fnmda, label='NMDA', c='red')
# plt.xlabel('Time (ms)', fontsize = 12)
# plt.ylabel('s variable', fontsize = 12)
# plt.legend()

plt.show()