#!/usr/bin/env python3

######################################
#       AdaptiveIF model BRIAN2      #
#           active dendrite          #
#                via                 #
#           custom events            #
#             FIG6B_Kim              #
######################################

from brian2 import *
import matplotlib.pyplot as plt
import numpy as nm

###############################################################################
#                 N e u r o n             p a r a m e t e r s                 #
###############################################################################

## population
N_GC = 1 # GC neurons of DG

## soma
Elsoma  = -80 * mV
Csoma 	= 70.* pfarad
glsoma 	= (Csoma / (26.9 * ms)) / 2.3
Vreset  = -74. * mV
Vthres  = 56.432 * mV 
print ('Soma Membrane time constant (Cm/gl) is : ', Csoma/glsoma)

## dendrite
Eldend  = (Elsoma + 5 * mV)
gldend  = 2. * glsoma
Cdend   = 2. * Csoma
print ('Dendrite membrane time (Cm/gl) constant is : ', Cdend/gldend)

## Global
gc      = 6.8 * nS

## AdIF
alpha   = 1.35 * nS
tauw    = 45. * ms
beta    = .045 * nA

## Na & K channels
tauNa   = 3. * ms
tauK = 6. * ms

###############################################################################
#                                M o d e l                                    #
###############################################################################    
   
eqs = '''
      dVsoma/dt = (-glsoma * (Vsoma - Elsoma) - gc * (Vsoma - Vdend) - w + Iextsoma) / Csoma : volt (unless refractory)
      dw/dt = (alpha * (Vsoma -Elsoma) - w ) / tauw : amp

      dVdend/dt = (-gldend * (Vdend - Eldend) - gc * (Vdend - Vsoma) + INa + IK + Iextdend) / Cdend : volt 
      dINa/dt = -INa / tauNa : amp
      dIK/dt = -IK / tauK : amp
   
      Iextdend : amp
      Iextsoma : amp
      allow_dspike : boolean
      '''

granule = NeuronGroup(1, model = eqs,
                     threshold = 'Vsoma > Vthres',
                     reset = 'Vsoma = Vreset; w += beta',
                     refractory = 2 * ms,
                     events = {
                           'dspike' : 'Vdend > -55 * mV and allow_dspike',
                           'drop_dspike' : 'Vdend > -37 * mV and not allow_dspike',
                           'force_true_dspike' : 'IK > -.009 * pA and not allow_dspike'
                           },
                     method = 'rk4'
                     )

# custom events conditions
granule.run_on_event('dspike', 'INa = 1000 * pA; allow_dspike = False')
granule.run_on_event('drop_dspike', 'IK = - 1600 * pA')
granule.run_on_event('force_true_dspike', 'allow_dspike = True')

# initializing all values.
granule.Vsoma = Elsoma
granule.Vdend = Eldend
granule.allow_dspike = True

###############################################################################
#                             M O N I T O R I N G                             #
###############################################################################

dt = defaultclock.dt

# recording variables, spikes and whatever needed.
spk = SpikeMonitor(granule)
  
t1=200*ms # time needed for equilibrium between dendrite and soma.
tstim = 5 * ms # stimulation duration.
t2 = 95 * ms # after stimulation end.
mon = StateMonitor(granule, ('Vsoma', 'Vdend', 'INa', 'IK', 'allow_dspike'), record=True)
   
# How the experiment is run.

run(t1)

granule.Iextsoma = 0. * pA
granule.Iextdend = 1000. * pA
run(tstim)

granule.Iextsoma = 0. * pA
granule.Iextdend = 0. * pA
run(t2)

###############################################################################
#                              P l o t t i n g                                #
###############################################################################

# Nicer spikes 
Vsoma_hack = mon[0].Vsoma[:]
for t in spk.t:
      ii = int(t / defaultclock.dt)-2000
      Vsoma_hack[ii] = 20 * mV
            
## Dendrite plot
plt.figure(1)
plt.tight_layout()
plt.plot(mon.t[1800:] / ms, mon[0].Vdend[1800:] / mV, label='dendrite', c='red')
plt.plot(mon.t[1800:] / ms, Vsoma_hack[1800:] / mV, label='soma', c='black')
plt.xlabel('Time (ms)')
plt.ylabel('Membrane Voltage (mV)')
plt.title('Dendritic Spike Mechanism')
plt.legend()

plt.show()