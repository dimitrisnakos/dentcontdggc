#!/usr/bin/env python3

######################################
#       AdaptiveIF model BRIAN2      #
#           active dendrite          #
#                via                 #
#           custom events            #
#           with SYNAPSES            #
#             VALIDATION             #
######################################

from brian2 import *
import matplotlib.pyplot as plt
import numpy as nm

###############################################################################
#                 N e u r o n             p a r a m e t e r s                 #
###############################################################################


## population
N_GC = 100 # GC neurons of DG

## soma
Elsoma  = -87 * mV
glsoma 	= 698. * psiemens
Csoma 	= 23. * pfarad
Vreset  = -74. * mV
Vthres  = -56.81 * mV 

## dendrite
Eldend  = -82 * mV 
gldend  = 610. * psiemens
Cdend   = 90. * pF

## Global
gcsd      = 0. * nS
gcds      = 0. * nS

### AMPA (excitatory)
E_AMPA      = 0. * mV
g_AMPA_ext  = .3839 * nS
tau_AMPA    = 2. * ms
tau_AMPA_rise     = .1 * ms
tau_AMPA_decay    = 2.5 * ms
alpha_AMPA        = .5 / ms

### NMDA (excitatory)
E_NMDA            = 0. * mV
g_NMDA_ext        = g_AMPA_ext * 1.08 #12.85545 *nS #.8711 * nS
tau_NMDA_rise     = .33 * ms
tau_NMDA_decay    = 50. * ms
alpha_NMDA        = .5 / ms
Mg2               = 2.
heta              = .2
gamma             = .04

### GABA (inhibitory) 
E_GABA            = -86. * mV
g_GABA_ext        = 1.25 * nS * 200. / N_GC
tau_GABA          = 10. * ms

## AdIF
alpha   = 1.6 * nS
tauw    = 45. * ms
beta    = .045 * nA 

## Na channels
tauNa   = 1. * ms


###############################################################################
#                                M o d e l                                    #
###############################################################################

eqs_gc = '''
    dVsoma/dt = (-glsoma * (Vsoma - Elsoma) - gcds * (Vsoma - Vdend) - w + Iextsoma - Isyns) / Csoma : volt (unless refractory)    
    dw/dt = (alpha * (Vsoma -Elsoma) -w ) / tauw : amp
    
    dVdend/dt = (-gldend * (Vdend - Eldend) - gcsd * (Vdend - Vsoma) + INa + Iextdend - Isynd) / Cdend :volt
    dINa/dt = -INa / tauNa : amp
    
    Isyns = I_GABA_ext + I_AMPA_ext : amp
    
    Isynd = I_AMPA_ext + I_NMDA_ext : amp
    
    I_AMPA_ext = g_AMPA_ext * (Vdend - E_AMPA) * s_AMPA_ext_tot : amp
    s_AMPA_ext_tot : 1
    
    I_NMDA_ext = g_NMDA_ext * (Vdend - E_NMDA) / (1 + heta * Mg2 * exp(gamma * Vdend / mV)) * s_NMDA_ext_tot : amp
    s_NMDA_ext_tot : 1
    
    I_GABA_ext = g_GABA_ext * (Vsoma - E_GABA) * s_GABA_ext : amp
    ds_GABA_ext / dt  = - s_GABA_ext / tau_GABA : 1
    
    Iextsoma : amp
    Iextdend : amp
    allow_dspike : boolean
    '''

granule = NeuronGroup(N_GC, model = eqs_gc, 
                     threshold = 'Vsoma > Vthres',
                     reset = 'Vsoma = Vreset; w += beta',
                     refractory= 2. * ms,
                     events = {
                     'dspike' : 'Vdend > -25 * mV and allow_dspike',
                     'drop_dspike' : 'Vdend > -.09 * mV and not allow_dspike'
                     },
                     method = 'euler'
                     )

# custom events conditions
granule.run_on_event('dspike', 'INa = 2500 * pA; allow_dspike = False')
granule.run_on_event('drop_dspike', 'Vdend = -35 * mV; allow_dspike = True')

###############################################################################
#                             S Y N A P S E S                                 #
###############################################################################

pois_Glut = PoissonGroup(500, rates = 0 * Hz)
pois_GABA = PoissonGroup(500, rates = 0 * Hz)
    
synGlut = Synapses(pois_Glut, granule,
                   model = """
                   s_AMPA_ext_tot_post = s_AMPA_ext : 1 (summed)
                   ds_AMPA_ext / dt = - s_AMPA_ext / tau_AMPA_decay + alpha_AMPA * x_AMPA * (1 - s_AMPA_ext) : 1 (clock-driven)
                   dx_AMPA / dt = - x_AMPA / tau_AMPA_rise : 1 (clock-driven)
                   
                   s_NMDA_ext_tot_post = s_NMDA_ext : 1 (summed)
                   ds_NMDA_ext / dt = - s_NMDA_ext / tau_NMDA_decay + alpha_NMDA * x_NMDA * (1 - s_NMDA_ext) : 1 (clock-driven)
                   dx_NMDA / dt = - x_NMDA / tau_NMDA_rise : 1 (clock-driven)
                   """,
                   on_pre = 'x_AMPA += 1; x_NMDA += 1',
                   delay = 3. * ms,
                   method = 'euler'
                   )

synGABA = Synapses(pois_GABA, granule,
                   on_pre = 's_GABA_ext += 1',
                   delay = 3. * ms,
                   method = 'euler'
                   )

synGlut.connect()
synGABA.connect()

# initializing all values.
granule.Vsoma = Elsoma
granule.Vdend = Eldend
granule.allow_dspike = True

###############################################################################
#                             M O N I T O R I N G                             #
###############################################################################

# print (I_AMPA_ext)

# recording variables, spikes and whatever needed.
mon = StateMonitor(granule, ('Vsoma', 'Vdend', 'INa'), record=True)
spk = SpikeMonitor(granule)


###############################################################################
#                            N E T W O R K   R U N                            #
###############################################################################
# Note to self: the run keeps all the values that PRECEDE it!!!
run(100 * ms, report = 'text') 

pois_Glut.rates = 8 * Hz
pois_GABA.rates = 2 * Hz
run(150 * ms, report = 'text')

pois_Glut.rates = 0 * Hz
pois_GABA.rates = 0 * Hz
run(200 * ms, report = 'text')
###############################################################################
#                        D a t a     G a t h e r i n g                        #
###############################################################################

# with open("active_dendrites_EPSP_glutamatergic.txt", "a") as myfile:
#     myfile.write(str(EPSP_amp)+",")
# print ("AMPA and NMDA provokes a", EPSP_amp, "somatic EPSP.")
# np.savetxt('synvalidationNMDA_dend.txt', mon[0].Vdend, header='data to use for doing syn validation (dendrite)')
# np.savetxt('synvalidationNMDA_soma.txt', mon[0].Vsoma, header='data to use for doing syn validation (soma)')

###############################################################################
#                              P l o t t i n g                                #
###############################################################################

print('I_NMDA_ext')

### Nicer somatic spikes 
Vsoma = mon[0].Vsoma[:]
for t in spk.t:
    some_value = int(t / defaultclock.dt)
    Vsoma[some_value] = 20*mV

### Somatic EPSP plot
# plt.plot()
plt.subplot(211)
plt.tight_layout()
plt.plot(mon.t / ms, Vsoma / mV, label='soma', c='blue')
plt.xlabel('Time (ms)', fontsize = 12)
plt.ylabel('EPSP (mV)', fontsize = 12)
plt.legend()

### Dendritic EPSP plot
# plt.plot()
# plt.subplot(312)
# plt.tight_layout()
# plt.plot(mon.t / ms, mon[0].Vdend / mV, label='dendrite', c='red')
# plt.xlabel('Time (ms)', fontsize = 12)
# plt.ylabel('EPSP (mV)', fontsize = 12)
# plt.legend()

### Raster plot of (soma?)
# plt.plot()
plt.subplot(212)
plt.tight_layout()
plt.plot(spk.t/ms, spk.i, '.k')
plt.xlabel('Time (ms)', fontsize = 12)
plt.ylabel('Neuron index', fontsize = 12)
plt.legend()

### INa plot
# plt.subplot(313)
# plt.tight_layout()
# plt.plot(mon.t / ms, mon[0].INa / nA, label='INa')
# plt.xlabel('Time (ms)', fontsize = 12)
# plt.ylabel('INa (nA)', fontsize = 12)
# plt.legend()

plt.show()