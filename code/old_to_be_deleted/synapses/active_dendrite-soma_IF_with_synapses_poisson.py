#!/usr/bin/env python3

######################################
#       AdaptiveIF model BRIAN2      #
#           active dendrite          #
#                via                 #
#           custom events            #
#           with SYNAPSES            #
######################################

from brian2 import *
import matplotlib.pyplot as plt
import numpy as nm

###############################################################################
#                 N e u r o n             p a r a m e t e r s                 #
###############################################################################


## population
N = 1000
N_GC = int(N * 0.8) # GC neurons of DG

## soma
Elsoma  = -87 * mV
glsoma 	= 698. * psiemens
Csoma 	= 23. * pfarad
Vreset  = -74. * mV
Vthres  = -56.81 * mV 

## dendrite
Eldend  = -82 * mV 
gldend  = 610. * psiemens
Cdend   = 90. * pF

## Global
gc      = 1.4 * nS

### AMPA (excitatory)
E_AMPA      = 0. * mV
g_AMPA_ext  = .3839 * nS
tau_AMPA    = 2. * ms
tau_AMPA_rise     = .1 * ms
tau_AMPA_decay    = 2.5 * ms
alpha_AMPA        = .5 / ms

### NMDA (excitatory)
E_NMDA            = 0. * mV
g_NMDA_ext        = g_AMPA_ext * 1.08 #12.85545 *nS #.8711 * nS
tau_NMDA_rise     = .33 * ms
tau_NMDA_decay    = 50. * ms
alpha_NMDA        = .5 / ms
Mg2               = 2.
heta              = .2
gamma             = .04

### GABA (inhibitory)
E_GABA            = -86. * mV
g_GABA_ext        = 1.25 * nS * 200. / N_GC
tau_GABA          = 10. * ms

## AdIF
alpha   = 1.6 * nS
tauw    = 45. * ms
beta    = 0.045 * nA 

## Na channels
tauNa   = 1. * ms

# AMPA (excitation)
## as external stimuli
rate = 3 * Hz # rate at which external activity comes at the synapses
C_ext = 900 # Number of excitatory connections

###############################################################################
#                                M o d e l                                    #
###############################################################################

eqs_gc = '''
    dVsoma/dt = (-glsoma * (Vsoma - Elsoma) - gc * (Vsoma - Vdend) - w + Iextsoma) / Csoma : volt (unless refractory)    
    dw/dt = (alpha * (Vsoma -Elsoma) -w ) / tauw : amp
    
    dVdend/dt = (-gldend * (Vdend - Eldend) -gc * (Vdend - Vsoma) + INa + Iextdend - Isyn) / Cdend :volt
    dINa/dt = -INa / tauNa : amp
    
    Isyn = I_AMPA_ext + I_NMDA_ext : amp
    
    I_AMPA_ext = g_AMPA_ext * (Vdend - E_AMPA) * s_AMPA_ext_tot : amp
    s_AMPA_ext_tot : 1
    
    I_NMDA_ext = g_NMDA_ext * (Vdend - E_NMDA) / (1 + heta * Mg2 * exp(gamma * Vdend / mV)) * s_NMDA_ext_tot : amp
    s_NMDA_ext_tot : 1
      
    Iextsoma : amp
    Iextdend : amp
    allow_dspike : boolean
    '''

granule = NeuronGroup(N_GC, model = eqs_gc, 
                        threshold = 'Vsoma > Vthres',
                        reset = 'Vsoma = Vreset; w += beta',
                        refractory= 2. * ms,
                        events = {
                            'dspike' : 'Vdend > -25 * mV and allow_dspike',
                            'drop_dspike' : 'Vdend > -.09 * mV and not allow_dspike'
                            },
                        method = 'euler'
                        )

# custom events conditions
granule.run_on_event('dspike', 'INa = 2500 * pA; allow_dspike = False')
granule.run_on_event('drop_dspike', 'Vdend = -35 * mV; allow_dspike = True')

###############################################################################
#                             S Y N A P S E S                                 #
###############################################################################

#indices = array([0, 1, 2, 3, 4, 5, 6, 7, 8])
#time = array([15, 100, 110, 112, 114, 200, 202, 204, 206]) * ms
#inp_ext = PoissonInput(granule,'s_AMPA_ext', C_ext, rate, '1', ) # invalid because it gives AttributeError: 'BinomialFunction' object has no attribute 'scalar'' when inserted at synapses
inp = PoissonGroup(1000, rates = 100 * Hz)

synAMPA = Synapses(inp, granule,
                    model = """
                    s_AMPA_ext_tot_post = s_AMPA_ext : 1 (summed)
                    ds_AMPA_ext / dt = - s_AMPA_ext / tau_AMPA_decay + alpha_AMPA * x_AMPA * (1 - s_AMPA_ext) : 1 (clock-driven)
                    dx_AMPA / dt = - x_AMPA / tau_AMPA_rise : 1 (clock-driven)
                    """,
                    on_pre = 'x_AMPA += 1',
                    method = 'euler'
                    )

synNMDA = Synapses(inp, granule,
                    model = """
                    s_NMDA_ext_tot_post = s_NMDA_ext : 1 (summed)
                    ds_NMDA_ext / dt = - s_NMDA_ext / tau_NMDA_decay + alpha_NMDA * x_NMDA * (1 - s_NMDA_ext) : 1 (clock-driven)
                    dx_NMDA / dt = - x_NMDA / tau_NMDA_rise : 1 (clock-driven)
                    """,
                    on_pre = 'x_NMDA += 1',
                    method = 'euler'
                    )

synAMPA.connect('i != j')
synNMDA.connect('i != j')

# initializing all values.
granule.Vsoma = Elsoma
granule.Vdend = Eldend
granule.allow_dspike = True

###############################################################################
#                             M O N I T O R I N G                             #
###############################################################################

# recording variables, spikes and whatever needed.
mon = StateMonitor(granule, ('Vsoma', 'Vdend', 'INa', 'allow_dspike'), record=True)
spk = SpikeMonitor(granule)

# How the experiment is run.
run(10 * ms, report='text')
granule.Iextsoma = 0. * nA
granule.Iextdend = 0. * nA
run(5 * ms, report='text')
granule.Iextsoma = 0. * nA
granule.Iextdend = 0. * nA
run(50 * ms, report='text')
granule.Iextsoma = 0. * nA
granule.Iextdend = 0. * nA
run(5 * ms, report='text')

###############################################################################
#                              P l o t t i n g                                #
###############################################################################

## Soma plot
# plt.plot()
plt.subplot(211)
# plt.tight_layout()
plt.plot(mon.t / ms, mon[0].Vsoma / mV, label='soma', c='blue')
plt.plot(mon.t / ms, mon[0].Vdend / mV, label='dendrite', c='green')
plt.title('NMDA and AMPA synapses input')
plt.xlabel('Time (ms)')
plt.ylabel('Membrane potential (mV)')
plt.legend()

### Na current plot
plt.subplot(212)
plt.tight_layout()
plt.plot(mon.t / ms, mon[0].INa / pA, label='INa', c='black')
plt.xlabel('Time (ms)')
plt.ylabel('Na Current (pA)')
plt.legend()

## boolean dspike plot
# plt.subplot(313)
# plt.tight_layout()
# plt.plot(mon.t / ms, mon[0].allow_dspike, label='boolean dspike', c='orange')
# plt.xlabel('Time (ms)')
# plt.ylabel('True/False dspike')
# plt.legend()

plt.show()
