#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

Vpeakdend = [0.,0.,0.,0.,0.,0.,0.,3.2501,8.7144,14.1787,19.6429,35.0226,34.2597,34.9848,34.9214]
Vpeaksoma = [0.,2.6111,5.2222,7.8334,10.4445,13.0556,15.6667,18.2778,20.8889,23.5,26.1111,25.2416,27.7486,24.9099,25.1495]
Iinj = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400]

fig, ax1 = plt.subplots()

color = 'tab:red'
ax1.set_xlabel('Current intensity (pA)', fontsize = 12)
ax1.set_ylabel('Peak amp. (mV)', fontsize = 12, color=color)
ax1.set_ylim(-1.5, 40)
ax1.plot(Iinj, Vpeakdend,
         'o-',
         color = color,
         label='Dendrite',
         )

ax2 = ax1.twinx() # instantiate a second axes that shares the same x-axis

color = 'tab:black'
ax2.set_ylabel('Peak amp. (mV)', fontsize = 12) # we already handled the x-label with ax1
ax2.set_ylim(-1.5, 50)
ax2.plot(Iinj, Vpeaksoma,
         '^-',
         color = 'black',
         label = 'soma',
         )

fig.legend(ncol=2, loc='upper right', bbox_to_anchor=(.43, .95, ))

fig.tight_layout() 
plt.show()