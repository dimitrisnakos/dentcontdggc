#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
#                         RE:  AdaptiveIF model BRIAN2                        #
#                               active dendrite                               #
#                                    via                                      #
#                                custom events                                #
#                                with SYNAPSES                                #
#                        VALIDATION  (Negative Currents)                      #
###############################################################################

from brian2 import *
import matplotlib.pyplot as plt
import numpy as np

###############################################################################
#                 N e u r o n             p a r a m e t e r s                 #
###############################################################################

start_scope()

## soma
Elsoma  = -87 * mV
glsoma 	= 699. * psiemens #698.95 * psiemens (It needs to be ~30* > Csoma)
Csoma 	= 24.* pfarad #23.907 * pfarad 
Vreset  = -74. * mV
Vthres  = -56. * mV 
print ('Soma Membrane time constant (Cm/gl) is : ', Csoma/glsoma)

## dendrite
Eldend  = (Elsoma + 5 * mV) # -82 * mV 
gldend  = 2. * glsoma #4.255 * glsoma #4 * glsoma # 610. * psiemens Default is: 1095.159 pS
Cdend   = 2. * Csoma #2.31 * Csoma #2 * Csoma # 90. * pF Default is: 2737.89 pF
print ('Dendrite membrane time (Cm/gl) constant is : ', Cdend/gldend)

## Global
gc      = 0. * nS # lower -> lowe tau, higher Rinput

## AdIF
alpha   = 0. * nS#1.8 * nS # sag ratio drops A LOT; Rinput drops A LOT when it goes higher.
tauw    = 45. * ms # lower -> higher sag. Rinp irrelevant
beta    = .045 * nA # doesn't change shit

## Validation/experiment parameters
tstart = 300 * ms
texprm = 1000 * ms
tend   = 600 * ms
Iinj   = -0.05 * nA 

###############################################################################
#                                M o d e l                                    #
###############################################################################

eqs_gc = '''
    dVsoma/dt = (-glsoma * (Vsoma - Elsoma) - gc * (Vsoma - Vdend) - w + Iextsoma ) / Csoma : volt (unless refractory)    
    dw/dt = (alpha * (Vsoma -Elsoma) -w ) / tauw : amp
    
    dVdend/dt = (-gldend * (Vdend - Eldend) -gc * (Vdend - Vsoma) + Iextdend) / Cdend :volt
     
    Iextsoma : amp
    Iextdend : amp
    '''

granule = NeuronGroup(1, model = eqs_gc,
                      method = 'euler',
                      threshold = 'Vsoma > Vthres',
                      reset = 'Vsoma = Vreset; w += beta',
                      refractory= 2. * ms,
                     )

# initializing all values in order for graphs to start from the desired values and NOT 0.
granule.Vsoma = Elsoma
granule.Vdend = Eldend

###############################################################################
#                             M O N I T O R I N G                             #
###############################################################################

# recording variables, spikes and whatever needed.
mon = StateMonitor(granule, ('Vsoma', 'Vdend'), record=True)

# emulating the start of the experiment.
run(tstart)

granule.Iextsoma = Iinj
granule.Iextdend = 0. * nA
run(texprm)

granule.Iextsoma = 0. * nA
granule.Iextdend = 0. * nA
run(tend)

###############################################################################
#                        D a t a     G a t h e r i n g                        #
###############################################################################

#print (mon[0].Vsoma)
Vsoma_vld = mon[0].Vsoma[:]

## Tau membrane calculation ##
# Time of data gathering needs to be a bit before the change of the curve.
tend = (1300 * ms) / defaultclock.dt #in our experiment the injection stops at 1300 ms. Converted to time within the simulator.
Nend = int(tend-1) # go 1 step BEFORE the stop of injection. 
Vtau = abs(mon[0].Vsoma[Nend:]) # Keep all the voltage needed for tau calculation.
Vnrm = (Vtau - min(Vtau)) / max(Vtau - min(Vtau)) # normalize to 1-0.
tau_m_debug = abs(Vnrm - 1/e) # used for debugging. 
tau_m = np.argmin(abs(Vnrm - 1/e)) * defaultclock.dt # 1/e = ~37%; argmin = index of minimum.

print ('Experimental tau membrane is : 26.9 ± 1.2 ms')
print ('Soma Membrane time constant in simulation is : ', tau_m)

# ## Rinput ##
# DV = Vsoma_vld[Nend] / mV - Vsoma_vld[2990] / mV
# Rin = (DV * mV) / Iinj
# print ('Experimental R input is : 228 ± 14.2 MΩ')
# print('R input is :', Rin)

# ## Sag ratio ##
# sag = DV / ( min(Vsoma_vld) / mV - Vsoma_vld[2990] / mV )
# print ('Experimental sag ratio is : 0.97 ± 0.01')
# print('sag ratio is :', sag)

###############################################################################
#                              P l o t t i n g                                #
###############################################################################
plt.figure(2)
plt.tight_layout()
plt.plot(mon.t / ms, mon[0].Vsoma / mV, label='soma', c='black')
plt.plot(mon.t / ms, mon[0].Vdend / mV, label='dendrite', c='red')
plt.xlabel('Time (ms)')
plt.ylabel('Membrane potential (mV)')
plt.legend()

plt.figure(1)
plt.tight_layout()
plt.plot(tau_m_debug, label='soma', c='black')
plt.hlines(1/e, 0, 6000, colors='green')
plt.plot(Vnrm, c='blue')
plt.xlabel('Index (Time * 10 ms)')
plt.ylabel('Normalized Membrane potential')
plt.legend()
plt.plot()

plt.show()