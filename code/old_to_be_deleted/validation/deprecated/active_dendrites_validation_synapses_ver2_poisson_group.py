#!/usr/bin/env python3

###############################################################################
#                           AdaptiveIF model BRIAN2                           #
#                               active dendrite                               #
#                                    via                                      #
#                                custom events                                #
#                                with SYNAPSES                                #
#                        VALIDATION  (Poisson & EPSP/#Syn)                    #
###############################################################################

from brian2 import *
import matplotlib.pyplot as plt
import numpy as np

# factor function used for normalization and min(=0), max(=1) during s_{AMPA,NMDA} graphs.
def factor(tau_rise,tau_decay):
    # Calculate the time @ peak
    tpeak = ((tau_decay*tau_rise) / (tau_decay-tau_rise)) * np.log(tau_decay/tau_rise)
    # Calculate the peak value - maximum value
    smax = ((tau_decay*tau_rise) / (tau_decay-tau_rise))*( np.exp(-(tpeak)/tau_decay) - np.exp(-(tpeak)/tau_rise) ) / ms
    
    return (1/smax)


###############################################################################
#                 N e u r o n             p a r a m e t e r s                 #
###############################################################################

start_scope()

## population
N_GC = 1 # GC neurons of DG

## soma
Elsoma  = -87 * mV
glsoma 	= 698. * psiemens
Csoma 	= 23. * pfarad
Vreset  = -74. * mV
Vthres  = -56.81 * mV 
print ('Soma Membrane time constant is : ', Csoma/glsoma)

## dendrite
Eldend  = (Elsoma + 5 * mV) # -82 * mV 
gldend  = 4 * glsoma # 610. * psiemens
Cdend   = 2 * Csoma # 90. * pF
print ('Dendrite membrane time constant is : ', Cdend/gldend)

## Global
gc      = 1.4 * nS

## AdIF
alpha   = 1.6 * nS
tauw    = 45. * ms
beta    = .045 * nA 

## Na channels
tauNa   = 1. * ms

### AMPA (excitatory)
E_AMPA            = 0. * mV
g_AMPA_ext        = .3639 * nS #.8077 * nS #.3839 * nS
tau_AMPA          = 2. * ms
tau_AMPA_rise     = .05 * ms
tau_AMPA_decay    = 1.5 * ms

### NMDA (excitatory)
E_NMDA            = 0. * mV
g_NMDA_ext        = .3746 * nS #g_AMPA_ext * 1.08 #12.85545 *nS #
tau_NMDA_rise     = 2. * ms#.33 * ms
tau_NMDA_decay    = 50. * ms #100. * ms
Mg2               = .05
heta              = .2
gamma             = .062

## Model irrelevant
tstart = 600 * ms
tend   = 800 * ms
fampa  = factor(tau_AMPA_rise,tau_AMPA_decay)
fnmda  = factor(tau_NMDA_rise,tau_NMDA_decay)

###############################################################################
#                                M o d e l                                    #
###############################################################################

eqs_gc = '''
    dVsoma/dt = (-glsoma * (Vsoma - Elsoma) - gc * (Vsoma - Vdend) - w + Iextsoma ) / Csoma : volt (unless refractory)    
    dw/dt = (alpha * (Vsoma -Elsoma) -w ) / tauw : amp
    
    dVdend/dt = (-gldend * (Vdend - Eldend) -gc * (Vdend - Vsoma) + INa + Iextdend - Isyn) / Cdend :volt
    dINa/dt = -INa / tauNa : amp
    
    Isyn = I_AMPA_ext + I_NMDA_ext : amp
    
    I_AMPA_ext = g_AMPA_ext * (Vdend - E_AMPA) * s_AMPA_ext * fampa : amp
    ds_AMPA_ext / dt = - s_AMPA_ext / tau_AMPA_decay + x_AMPA / ms : 1 # by using /ms we eradicate usage of alpha
    dx_AMPA / dt = - x_AMPA / tau_AMPA_rise : 1
    
    I_NMDA_ext = g_NMDA_ext * (Vdend - E_NMDA) / (1 + Mg2 / heta * exp(gamma * Vdend / mV)) * s_NMDA_ext * fnmda : amp
    ds_NMDA_ext / dt = - s_NMDA_ext / tau_NMDA_decay + x_NMDA / ms: 1 
    dx_NMDA / dt = - x_NMDA / tau_NMDA_rise : 1 
    
    Iextsoma : amp
    Iextdend : amp
    allow_dspike : boolean
    '''

granule = NeuronGroup(1, model = eqs_gc, 
                     threshold = 'Vsoma > Vthres',
                     reset = 'Vsoma = Vreset; w += beta',
                     refractory= 2. * ms,
                     events = {
                     'dspike' : 'Vdend > -25 * mV and allow_dspike',
                     'drop_dspike' : 'Vdend > -.09 * mV and not allow_dspike'
                     },
                     method = 'rk4'
                     )

# custom events conditions
granule.run_on_event('dspike', 'INa = 2500 * pA; allow_dspike = False')
granule.run_on_event('drop_dspike', 'Vdend = -35 * mV; allow_dspike = True')

###############################################################################
#                             S Y N A P S E S                                 #
###############################################################################

# In this case we use PoissonInput for synaptic validation.

spike_inp = PoissonGroup(10, rates = 10 * Hz)

synAMPA = Synapses(spike_inp, 
                    granule,
                    on_pre = 'x_AMPA += 1',
                    delay = 3 * ms
                    )

synNMDA = Synapses(spike_inp, 
                   granule,
                   on_pre = 'x_NMDA += 1',
                   delay = 3 * ms
                   )

synAMPA.connect(p = 1.)
synNMDA.connect(p = 1.)

# initializing all values.
granule.Vsoma = Elsoma
granule.Vdend = Eldend
granule.allow_dspike = True

###############################################################################
#                             M O N I T O R I N G                             #
###############################################################################

spk = SpikeMonitor(granule)

# I want to record values AFTER 300 ms of simulation, because dendrite-soma 
# membranes come to "equilibrium around then. It's easier to calculate min and max values
# this way.
mon = StateMonitor(granule, ('Vsoma', 'Vdend', 'I_AMPA_ext', 'I_NMDA_ext', 's_AMPA_ext', 's_NMDA_ext'), record=True)

spike_inp.rates = 0 * Hz
run(300 *ms, report = 'text')

# recording variables, spikes and whatever needed.
# mon = StateMonitor(granule, ('Vsoma', 'Vdend', 'I_AMPA_ext', 'I_NMDA_ext', 's_AMPA_ext', 's_NMDA_ext'), record=True)
spike_inp.rates = 10 * Hz
granule.Iextsoma = 0. * nA
granule.Iextdend = 0. * nA
run(700 * ms, report = 'text')

# granule.Iextsoma = 0.27 * nA
# granule.Iextdend = 0. * nA
# run(500 * ms)

nstart      = int(tstart/defaultclock.dt)
nend        = int(tend/defaultclock.dt)
###############################################################################
#                        D a t a     G a t h e r i n g                        #
###############################################################################
print (mon[0].Vsoma)
EPSP_amp = max(mon[0].Vsoma[nstart:nend]) - min(mon[0].Vsoma[nstart:nend])
# print (max(mon[0].Vsoma), min(mon[0].Vsoma))
print ("AMPA and NMDA provokes a", EPSP_amp, "somatic EPSP.")
print ("ratio of I_NMDA/I_AMPA is: ", max(abs(mon[0].I_NMDA_ext)) / max(abs(mon[0].I_AMPA_ext)))
# np.savetxt('synvalidationNMDA_dend.txt', mon[0].Vdend, header='data to use for doing syn validation (dendrite)')
# np.savetxt('synvalidationNMDA_soma.txt', mon[0].Vsoma, header='data to use for doing syn validation (soma)')

###############################################################################
#                              P l o t t i n g                                #
###############################################################################

### Nicer somatic spikes
Vsoma = mon[0].Vsoma[:]
for t in spk.t : 
      some_value = int(t / defaultclock.t)
      Vsoma[some_value] = 20 * mV

# plt.figure()
# plt.plot()q
plt.subplot(323)
plt.tight_layout()
plt.plot(mon[0].t[nstart:nend] / ms, mon[0].Vsoma[nstart:nend] / mV, label='soma', c='black')
# plt.plot(mon[0].t[nstart:nend] / ms, mon[0].Vdend[nstart:nend] / mV, label='dendrite', c='red')
# plt.vlines(453, -85.61975611, -86.21980416, colors='c',linestyles='dashed', label='amplitude')
# plt.text(465, -86, '0.6 mV', fontsize = 10)
# plt.plot(mon.t / ms, mon[0].Vdend / mV, label='dendrite', c='green')
plt.xlabel('Time (ms)', fontsize = 12)
plt.ylabel('EPSP (mV)', fontsize = 12)
# plt.ylim([39.5, 41.5])
# plt.text(400, 200, "Excited Soma at +40 mV membrane potential with one dendrite. Only NMDA active", fontsize = 20)
# plt.ylim([-90, 125])
plt.legend()

plt.subplot(324)
plt.tight_layout()
plt.plot(mon[0].t[nstart:nend] / ms, mon[0].Vdend[nstart:nend] / mV, label='dendrite', c='red')
# plt.vlines(453, -85.61975611, -86.21980416, colors='c',linestyles='dashed', label='amplitude')
# plt.text(465, -86, '0.6 mV', fontsize = 10)
# plt.plot(mon.t / ms, mon[0].Vdend / mV, label='dendrite', c='green')
plt.xlabel('Time (ms)', fontsize = 12)
plt.ylabel('EPSP (mV)', fontsize = 12)
# plt.ylim([-41.5, -38])
# plt.ylim([-90, 125])
plt.legend()

      
### Soma and dendrite plot
plt.subplot(311)
plt.tight_layout()
plt.plot(mon[0].t[:] / ms, mon[0].Vsoma[:] / mV, label='soma', c='black')
plt.plot(mon[0].t[:] / ms, mon[0].Vdend[:] / mV, label='dendrite', c='red')
# plt.vlines(453, -85.61975611, -86.21980416, colors='c',linestyles='dashed', label='amplitude')
# plt.text(465, -86, '0.6 mV', fontsize = 10)
# plt.plot(mon.t / ms, mon[0].Vdend / mV, label='dendrite', c='green')
plt.xlabel('Time (ms)', fontsize = 12)
plt.ylabel('EPSP (mV)', fontsize = 12)
plt.title('EPSP and EPSC with AMPA and NMDA from 10 neurons', fontsize=16)
# plt.text(400, 200, "Excited Soma at +40 mV membrane potential with one dendrite. Only NMDA active", fontsize = 20)
# plt.ylim([-90, 125])
plt.legend()

# plt.figure()
# plt.plot()
plt.subplot(313)
plt.tight_layout()
plt.plot(mon.t / ms, mon[0].I_AMPA_ext[:] / pA, label='AMPA', c='blue')
plt.plot(mon.t / ms, mon[0].I_NMDA_ext[:] / pA, label='NMDA', c='green')
plt.xlabel('Time (ms)', fontsize = 12)
plt.ylabel('EPSC (pA)', fontsize = 12)
# plt.ylim([-20, 0])
plt.legend()

# plt.figure()
# plt.plot()
# # plt.subplot(211)
# # plt.tight_layout()
# plt.plot(mon.t / ms, mon[0].s_AMPA_ext[:] * fampa, label='AMPA', c='blue')
# plt.plot(mon.t / ms, mon[0].s_NMDA_ext[:] * fnmda, label='NMDA', c='red')
# plt.xlabel('Time (ms)', fontsize = 12)
# plt.ylabel('s variable', fontsize = 12)
# plt.legend()

plt.show()