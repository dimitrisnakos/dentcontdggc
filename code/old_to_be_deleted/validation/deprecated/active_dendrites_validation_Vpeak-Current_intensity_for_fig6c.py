#!/usr/bin/env python3

######################################
#       AdaptiveIF model BRIAN2      #
#           active dendrite          #
#                via                 #
#           custom events            #
#             FIG6C_Kim              #
######################################

from brian2 import *
import matplotlib.pyplot as plt
import numpy as nm

###############################################################################
#                 N e u r o n             p a r a m e t e r s                 #
###############################################################################

## population
N_GC = 1 # GC neurons of DG

## soma
Elsoma  = -87 * mV
glsoma 	= 698. * psiemens
Csoma 	= 23. * pfarad
Vreset  = -74. * mV
Vthres  = -56.81 * mV 

tw = Csoma / glsoma

## dendrite
Eldend  = -82 * mV 
gldend  = 610. * psiemens
Cdend   = 90. * pF

## Global
gcds      = 1.4 * nS # coupling bAP FROM dendrite to soma
gcsd      = 0. * nS  # coupling bAP FROM soma to dendrite

## AdIF
alpha   = 1.6 * nS
tauw    = 45. * ms
beta    = 0.045 * nA 

## Na channels
tauNa   = 1. * ms

# Global declarations
Vpeak_soma = []
Vpeak_dend = []
f = []
vmdendall = {} # dictionary that will keep all currents and all voltages produced by each

# currentIVDend = list(np.arange(0., 1250., 100.))

###############################################################################
#                                M o d e l                                    #
###############################################################################

# for i in currentIVDend:
    
#       start_scope()        
   
eqs = '''
      dVsoma/dt = (-glsoma * (Vsoma - Elsoma) - gcds * (Vsoma - Vdend) - w + Iextsoma) / Csoma : volt (unless refractory)
      dw/dt = (alpha * (Vsoma -Elsoma) - w ) / tauw : amp

      dVdend/dt = (-gldend * (Vdend - Eldend) - gcsd * (Vdend - Vsoma) + INa + Iextdend) / Cdend : volt 
      dINa/dt = -INa / tauNa : amp
   
      Iextdend : amp
      Iextsoma : amp
      allow_dspike : boolean
      '''

neuron = NeuronGroup(1, model = eqs,
                     threshold = 'Vsoma > Vthres',
                     reset = 'Vsoma = Vreset; w += beta',
                     refractory = 2 * ms,
                     events = {
                           'dspike' : 'Vdend > -25 * mV and allow_dspike',
                           'drop_dspike' : 'Vdend > .09 * mV and not allow_dspike'
                           },
                     method = 'euler'
                     )

# custom events conditions
neuron.run_on_event('dspike', 'INa = 2500 * pA; allow_dspike = False')
neuron.run_on_event('drop_dspike', 'Vdend = -35 * mV; allow_dspike = True')

# initializing all values.
neuron.Vsoma = Elsoma
neuron.Vdend = Eldend
neuron.allow_dspike = True

###############################################################################
#                             M O N I T O R I N G                             #
###############################################################################

dt = defaultclock.dt

# recording variables, spikes and whatever needed.
# mon = StateMonitor(neuron, ('Vsoma', 'Vdend', 'INa', 'allow_dspike'), record=True)
spk = SpikeMonitor(neuron)
          
# How the experiment is run.

t1=200*ms # time needed for equilibrium between dendrite and soma.
tstim = 5 * ms
t2 = 95 * ms

run(t1)#, report='text')

# After the equilibrium I start monitoring values, to have the best Vpeak values.
mon = StateMonitor(neuron, ('Vsoma', 'Vdend', 'INa', 'allow_dspike'), record=True)
neuron.Iextsoma = 0. * pA
neuron.Iextdend = 1400. * pA
run(tstim)#, report='text')

neuron.Iextsoma = 0. * pA
neuron.Iextdend = 0. * pA
run(t2)#, report='text')
#    print ('External current is', round(i, 3), 'nA')
    

###############################################################################
#                              P l o t t i n g                                #
###############################################################################

# Nicer spikes 
Vsoma = mon[0].Vsoma[:]
for t in spk.t:
      ii = int(t / defaultclock.dt)
      Vsoma[ii] = 20 * mV
      
Vdend = mon[0].Vdend[:]
          
## Vpeak array creation ##
Vpeak_soma.append(max(Vsoma) / mV - min(Vsoma) / mV)
Vpeak_dend.append(max(Vdend) / mV - min(Vdend) / mV)

print (Vpeak_dend)
print (Vpeak_soma)
## Creating array f that holds all times of somatic spikes ##
f.append(spk.num_spikes / 1)

# Vpeak_dendrite = max(Vdend-Vdend[int(t1/dt)-1])

# vmdendall['Current_'+str(i)] = Vdend-Vdend[int(t1/dt)-1]

# np.savetxt('all_dend_experiments.txt', Vpeak_dendrite)

#### Thelw na kratisw mono to Vmax apo kathe periptwsi!!! #####

# np.savetxt('all_dend_experiments'+str(i)+'.txt', Vpeak_dendrite, fmt='%.8e')

## Figure to see how spikes progress to be able to gather data.
# print ('value printed: ', Vpeak_dendrite)
# print (vmdendall)
# with open("soma_Vpeak.txt", "a") as myfile:
#     myfile.write(str(nm.round(Vpeak_soma,4))+",")
    
# with open("dendrite_Vpeak.txt", "a") as myfile:
#     myfile.write(str(nm.round(Vpeak_dend,4))+",")
    

# EPSP_amp = max(mon[0].Vsoma - min(mon[0].Vsoma)) / mV
# print (max(mon[0].Vdend))
# print (Vdend)
# print (Vpeak_soma)

### I-V plot
# plt.figure(1)
# # plt.subplot(211)
# plt.plot(currentIVDend, Vpeak_soma, '^-', label='soma', c='black')
# plt.plot(currentIVDend, Vpeak_dend, 'o-', label='dendrite', c='red')
# # plt.plot([],[], linestyle = ' ', label = "Currents provided: 0 to 1000 pA\nStepsize: 100 pA")
# plt.xlabel('Current intensity (pA)')
# plt.ylabel('Peak amp. (mV)')
# plt.legend()

plt.figure(1)
# plt.subplot(211)
# plt.tight_layout()
plt.plot(mon.t / ms, Vdend / mV, label='dendrite', c = 'red')
plt.plot(mon.t / ms, Vsoma / mV, label='soma', c='black')
plt.xlabel('Time (ms)')
plt.ylabel('VCurrent (pA)')
# plt.legend()


### Dendrite plot
# plt.figure(2)
# # plt.subplot(211)
# plt.tight_layout()
# plt.plot(mon.t / ms, mon[0].Vdend / mV, label='dendrite', c='red')
# plt.plot(mon.t / ms, mon[0].Vsoma / mV, label='soma', c='black')
# plt.xlabel('Time (ms)')
# plt.ylabel('VCurrent (pA)')
# plt.legend()

## Vpeak plot
# plt.plot(2)
# # plt.subplot(413)
# plt.tight_layout()
# plt.plot(mon.t / ms, Vpeak / mV, label='Vpeak', c='purple')
# plt.xlabel('Time (ms)')
# plt.ylabel('Vpeak (mV)')

## Na current plot
# plt.figure(2)
# plt.subplot(212)
# plt.tight_layout()
# plt.plot(mon.t / ms, mon[0].INa / pA, label='INa', c='blue')
# plt.xlabel('Time (ms)')
# plt.ylabel('VCurrent (pA)')
# plt.legend()

## Dspike plot 
#plt.subplot(414)
#plt.tight_layout()
#plt.plot(mon.t / ms, mon[0].allow_dspike, label='boolean dspike', c='orange')
#plt.xlabel('Time (ms)')
#plt.ylabel('Vpeak')

plt.show()