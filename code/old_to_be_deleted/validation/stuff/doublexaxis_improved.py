#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

EPSP_measured = [0.60004873, 1.18820132, 1.76473778, 2.32993164, 2.88404971, 3.42735151, 3.96009003, 4.48251263, 4.99485963, 5.49736542, 5.99025847, 6.47376142, 6.94809127,7.41345946, 7.87007205, 8.31812983]
EPSP_expected = [0.60004873, 0.60004873 * 2, 0.60004873 * 3, 0.60004873 * 4, 0.60004873 * 5, 0.60004873 * 6, 0.60004873 * 7, 0.60004873 * 8, 0.60004873 * 9, 0.60004873 * 10, 0.60004873 * 11, 0.60004873 * 12, 0.60004873 * 13, 0.60004873 * 14, 0.60004873 * 15, 0.60004873 * 16]
synapse = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
x = np.linspace(0, 10, 50)
y1 = x
y2 = 1.44 * x

fig, ax1 = plt.subplots()

color = 'tab:red'
ax1.set_xlabel('Expected (mV)', fontsize = 12)
ax1.set_ylabel('Measured (mV)', fontsize = 12)
ax1.scatter(EPSP_expected, EPSP_measured,
            color = color,
            label='somatic EPSP, with active dendrites',
            )

ax1.plot(x, y2,
         color = 'black',
         label = 'gain (y= 1.44 x)'
         )

ax1.plot(x, y1,
         color = 'grey',
         linewidth = 1.0,
         linestyle = '--',
         label = 'linear summation'
         )

ax1.set_xlim(xmin=0)
ax1.set_ylim(ymin=0)

ax2 = ax1.twiny() # instantiate a second axes that shares the same y-axis

color = 'tab:blue'
ax2.set_xlabel('# of Synapses', fontsize = 12) # we already handled the x-label with ax1
ax2.scatter(synapse, EPSP_measured,
            color = color,
            label = 'somatic EPSP, per number of synapses',
            )


fig.legend(ncol=2, loc='upper center', bbox_to_anchor=(0.5, 0.85, ))


