#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

EPSP_measuered = [0.60004873, 1.18820132, 1.76473777, 2.32993164, 2.88404970, 3.42735151, 3.96009002, 4.48251262, 4.99485962, 5.49736541, 5.99025846, 6.47376142]
EPSP_expected = [0.60004873, 0.60004873 * 2, 0.60004873 * 3, 0.60004873 * 4, 0.60004873 * 5, 0.60004873 * 6, 0.60004873 * 7, 0.60004873 * 8, 0.60004873 * 9, 0.60004873 * 10, 0.60004873 * 11, 0.60004873 * 12]
synapse = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
x = np.linspace(0, 7, 50)
y1 = x
y2 = 1.44 * x

plt.plot()
plt.scatter(EPSP_measuered, EPSP_expected, 
            label='somatic EPSP, inactive dendrites, AMPA and NMDA', c='blue'
            )
plt.plot(x, y2,
         color = 'green',
         label = 'gain (y= 1.44 x)'
         )
plt.plot(x, y1,
         color = 'red',
         linewidth = 1.0,
         linestyle = '--',
         label = 'linear summation'
         )
plt.xlabel('Expected (mV)', fontsize = 12)
plt.ylabel('Measured (mV)', fontsize = 12)
plt.legend()

plt.show()
