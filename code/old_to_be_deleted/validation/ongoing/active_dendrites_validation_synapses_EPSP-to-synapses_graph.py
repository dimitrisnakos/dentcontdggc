#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

EPSP_measuered = [3.02841667,8.10579041,12.16288425,15.45257455,18.15604037,20.405372,22.29854661,23.9090694,25.29281796,26.49270407,27.54198648,28.46675942,28.72541218,28.73812899,28.67338286]
synapse = [1,3,5,7,9,11,13,15,17,19,21,23,25,27,29]
# x = np.linspace(0, 7, 50)
# y1 = x
# y2 = 1.44 * x

plt.plot()
plt.scatter(synapse, EPSP_measuered, 
            label='AMPA + NMDA, somatic EPSP, active dendrites', c='blue'
            )
# plt.plot(x, y2,
#          color = 'green',
#          label = 'gain (y= 1.44 x)'
#          )
# plt.plot(x, y1,
#          color = 'red',
#          linewidth = 1.0,
#          linestyle = '--',
#          label = 'linear summation'
#          )
plt.title("Somatic EPSP with active dendrites, NMDA and AMPA synapses. SpikeGenerator usage", fontsize = 14)
plt.xlabel('# Synapses', fontsize = 12)
plt.ylabel('EPSP (mV)', fontsize = 12)
plt.legend()

plt.show()
