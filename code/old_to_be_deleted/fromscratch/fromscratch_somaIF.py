#!/usr/bin/env python3

######################################
#       AdaptiveIF model BRIAN2      #
#               soma                 #
######################################

from brian2 import *
import matplotlib.pyplot as plt
import numpy as nm

# Neuron parameters
El      = -87 * mV
gl      = 30. * nS 
Cs      = 100. *pF 
Vreset  = -74. * mV
Vthres  = -56. * mV 

alpha   = 2. * nS
tauw    = 45. * ms
beta    = 0.045 * nA 

# Model
eqs = '''
    dVsoma/dt = (-gl * (Vsoma - El) - w + Iext) / Cs : volt
    dw/dt = (alpha * (Vsoma -El) -w ) / tauw : amp
    Iext : amp
    '''

neuron = NeuronGroup(1, model = eqs, 
                        threshold = 'Vsoma > Vthres',
                        reset = 'Vsoma = Vreset; w += beta',
                        refractory = 3 * ms,
                        method = 'rk4'
                        )
# initializing all values.
neuron.Vsoma = El

# recording variables, spikes and whatever needed.
mon = StateMonitor(neuron, 'Vsoma', record=True)
spk = SpikeMonitor(neuron)

# How the experiment is run.
run(10 * ms, report='text')
neuron.Iext = 1. * nA
run(50 * ms, report='text')
neuron.Iext = 0. * nA
run(100 * ms, report='text')

# plots that correspont to experiment results.
plt.plot(mon.t / ms, mon[0].Vsoma / mV, label='soma')
plt.xlabel('Time (ms)')
plt.ylabel('Membrane potential (mV)')
plt.legend()
plt.show()
