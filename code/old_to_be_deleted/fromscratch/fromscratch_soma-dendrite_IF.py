#!/usr/bin/env python3

######################################
#       AdaptiveIF model BRIAN2      #
#         soma and 1 dendrite        #
######################################

from brian2 import *
import matplotlib.pyplot as plt
import numpy as nm

# Neuron parameters
## soma
Elsoma  = -87 * mV
glsoma  = 30. * nS 
Csoma   = 100. *pF 
Vreset  = -74. * mV
Vthres  = -56. * mV 

## dendrite
Eldend  = -82 * mV 
gldend  = 10. * nS
Cdend   = 250. * pF

gc      = 10 * nS

## AdIF
alpha   = 2. * nS
tauw    = 45. * ms
beta    = 0.045 * nA 

# Model
eqs = '''
    dVdend/dt = (-gldend * (Vdend - Eldend) -gc * (Vdend - Vsoma)) / Cdend :volt
    dVsoma/dt = (-glsoma * (Vsoma - Elsoma) - gc * (Vsoma - Vdend) - w + Iext) / Csoma : volt
    dw/dt = (alpha * (Vsoma -Elsoma) -w ) / tauw : amp
    Iext : amp
    '''

neuron = NeuronGroup(1, model = eqs, 
                        threshold = 'Vsoma > Vthres',
                        reset = 'Vsoma = Vreset; w += beta',
                        method = 'rk4'
                        )
# initializing all values.
neuron.Vsoma = Elsoma
neuron.Vdend = Eldend

# recording variables, spikes and whatever needed.
mon = StateMonitor(neuron, ('Vsoma', 'Vdend'), record=True)
spk = SpikeMonitor(neuron)

# How the experiment is run.
run(10 * ms, report='text')
neuron.Iext = 1.2 * nA
run(50 * ms, report='text')
neuron.Iext = 0. * nA
run(100 * ms, report='text')

# plots that correspont to experiment results.
plt.subplot(211)
plt.tight_layout()
plt.plot(mon.t / ms, mon[0].Vsoma / mV, label='soma', c='blue')
plt.xlabel('Time (ms)')
plt.ylabel('Membrane potential (mV)')
plt.legend()

plt.subplot(212)
plt.tight_layout()
plt.plot(mon.t / ms, mon[0].Vdend / mV, label='dendrite', c='green')
plt.xlabel('Time (ms)')
plt.ylabel('Membrane potential (mV)')
plt.legend()

plt.show()
