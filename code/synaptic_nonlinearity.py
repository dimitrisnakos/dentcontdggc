#!/usr/bin/env python3

######################################
#       AdaptiveIF model BRIAN2      #
#           active dendrite          #
#                via                 #
#           custom events            #
#           with SYNAPSES            #
#             VALIDATION             #
######################################

from brian2 import *
import matplotlib.pyplot as plt
import numpy as nm

###############################################################################
#                 N e u r o n             p a r a m e t e r s                 #
###############################################################################


## population
N_GC = 1 # GC neurons of DG

## soma
Elsoma  = -80 * mV
Csoma 	= 70.* pfarad 
glsoma 	= (Csoma / (26.9 * ms)) / 2.3 
Vreset  = -74. * mV
Vthres  = 56. * mV 
print ('Soma Membrane time constant (Cm/gl) is : ', Csoma/glsoma)

## dendrite
Eldend  = (Elsoma + 5 * mV)
gldend  = 2. * glsoma
Cdend   = 2. * Csoma
print ('Dendrite membrane time (Cm/gl) constant is : ', Cdend/gldend)

## Global
gc      = 6.8 * nS 

### AMPA (excitatory)
E_AMPA      = 0. * mV
g_AMPA_ext  = 2.1066 * nS
tau_AMPA    = 2. * ms
tau_AMPA_rise     = .073 * ms
tau_AMPA_decay    = 2. * ms
alpha_AMPA        = .68 / ms

### NMDA (excitatory)
E_NMDA            = 0. * mV
g_NMDA_ext        = .99 * g_AMPA_ext 
tau_NMDA_rise     = .33 * ms
tau_NMDA_decay    = 50. * ms
alpha_NMDA        = .22 / ms
Mg2               = 2.
heta              = .2
gamma             = .04

## AdIF
alpha   = 1.35 * nS
tauw    = 45. * ms 
beta    = .045 * nA 

## Na & K channels
tauNa   = 3. * ms
tauK = 6. * ms

dt = defaultclock.dt
EPSP_measured=[0]
EPSP_expected=[0]

###############################################################################
#                                M o d e l                                    #
###############################################################################

eqs_gc = '''
    dVsoma/dt = (-glsoma * (Vsoma - Elsoma) - gc * (Vsoma - Vdend) - w + Iextsoma) / Csoma : volt (unless refractory)    
    dw/dt = (alpha * (Vsoma -Elsoma) -w ) / tauw : amp
    
    dVdend/dt = (-gldend * (Vdend - Eldend) -gc * (Vdend - Vsoma) + INa + IK + Iextdend - Isyn) / Cdend :volt
    dINa/dt = -INa / tauNa : amp
    dIK/dt = -IK / tauK : amp
    
    Isyn = I_AMPA_ext + I_NMDA_ext : amp
    
    I_AMPA_ext = g_AMPA_ext * (Vdend - E_AMPA) * s_AMPA_ext_tot : amp
    s_AMPA_ext_tot : 1
    
    I_NMDA_ext = g_NMDA_ext * (Vdend - E_NMDA) / (1 + heta * Mg2 * exp(gamma * Vdend / mV)) * s_NMDA_ext_tot : amp
    s_NMDA_ext_tot : 1
    
    Iextsoma : amp
    Iextdend : amp
    allow_dspike : boolean
    '''


###############################################################################
#                             S Y N A P S E S                                 #
###############################################################################

indices = []
times = []

# For validation only one spike needs to be used, but from many different synapses.
for N in range(1, 35) :
    indices = list(range(0,N))
    times = ([400] * (N)) * ms
    spike_inp = SpikeGeneratorGroup(N, indices, times)
    
    
    
    granule = NeuronGroup(1, model = eqs_gc, 
                     threshold = 'Vsoma > Vthres',
                     reset = 'Vsoma = Vreset; w += beta',
                     refractory= 2. * ms,
                     events = {
                    'dspike' : 'Vdend > -55 * mV and allow_dspike',
                    'drop_dspike' : 'Vdend > -37 * mV and not allow_dspike',
                    'force_true_dspike' : 'IK > -.009 * pA and not allow_dspike'
                     },
                     method = 'rk4'
                     )

# custom events conditions
    granule.run_on_event('dspike', 'INa = 1000 * pA; allow_dspike = False')
    granule.run_on_event('drop_dspike', 'IK = - 1600 * pA') #'; allow_dspike = True')
    granule.run_on_event('force_true_dspike', 'allow_dspike = True')
    
    # initializing all values.
    granule.Vsoma = Elsoma
    granule.Vdend = Eldend
    granule.allow_dspike = True
    
# print (indices)
    synGlut = Synapses(spike_inp, granule,
                       model = """
                       s_AMPA_ext_tot_post = s_AMPA_ext : 1 (summed)
                       ds_AMPA_ext / dt = - s_AMPA_ext / tau_AMPA_decay + alpha_AMPA * x_AMPA * (1 - s_AMPA_ext) : 1 (clock-driven)
                       dx_AMPA / dt = - x_AMPA / tau_AMPA_rise : 1 (clock-driven)
                       
                       s_NMDA_ext_tot_post = s_NMDA_ext : 1 (summed)
                       ds_NMDA_ext / dt = - s_NMDA_ext / tau_NMDA_decay + alpha_NMDA * x_NMDA * (1 - s_NMDA_ext) : 1 (clock-driven)
                       dx_NMDA / dt = - x_NMDA / tau_NMDA_rise : 1 (clock-driven)
                       """,
                       on_pre = 'x_AMPA += 1; x_NMDA += 1',
                       method = 'rk4'
                       )
    
    synGlut.connect()
    
    # initializing all values.
    granule.Vsoma = Elsoma
    granule.Vdend = Eldend
    granule.allow_dspike = True
    
###############################################################################
#                             M O N I T O R I N G                             #
###############################################################################
    
    # I want to record values AFTER 350 ms of simulation, because dendrite-soma 
    # membranes come to "equilibrium around then. It's easier to calculate min and max values
    # this way.
    mon = StateMonitor(granule, ('Vsoma', 'Vdend', "I_NMDA_ext", "I_AMPA_ext"), record=True)

    granule.Iextsoma = 0. * nA
    granule.Iextdend = 0. * nA
    run(800 *ms, report = 'text')
     
    EPSP_amp = max(mon[0].Vsoma[300:] - min(mon[0].Vsoma[300:])) / mV
    EPSP_measured.append(EPSP_amp)
    EPSP_exp = EPSP_measured[1]*N
    EPSP_expected.append(EPSP_exp)
    
    ## For debugging:
    # print ("Somatic Vpeak is: ", EPSP_amp)
    # print ("I_NMDA is: ", mon[0].I_NMDA_ext)
    # print ("I_AMPA is: ", mon[0].I_AMPA_ext)
   
    
###############################################################################
#                              P l o t t i n g                                #
###############################################################################
    
  
x = np.linspace(0, 30, 50)
y1 = x
y2 = 1.44 * x

fig, ax1 = plt.subplots()

ax1.set_xlabel('Expected (mV)', fontsize = 12)
ax1.set_ylabel('Measured (mV)', fontsize = 12)

ax1.plot(EPSP_expected, EPSP_measured,'o-b', 
            label='somatic EPSP, active dendrites, AMPA and NMDA', c='blue'
            )

ax1.plot(x, y2,
         color = 'black',
         label = 'gain (y= 1.44 x)'
         )

ax1.plot(x, y1,
         color = 'gray',
         linewidth = 1.0,
         linestyle = '--',
         label = 'linear summation'
         )

ax1.set_xlim(xmin=0)
ax1.set_ylim(ymin=0)

plt.legend()

plt.show()